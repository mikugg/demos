## Miku terminal-demo

#### Setup

1. Copy `.env.example` to a new `.env` file and add your API keys:

```
OPENAI_API_KEY=sk-your-key
ELEVEN_KEY=your-eleven-labs-api-key
ELEVEN_VOICE_KEY=eleven-labs-voice-key
WHISPER_AUDIO_API=whisper-api-url-from-hugging-face
WHISPER_AUDIO_API_KEY=whisper-api-key-from-hugging-face
```

2. Install system dependencies

Ubuntu:
```
apt install sox libsox-fmt-all
```

Mac:
```
brew install sox
```

3. Run `npm install`
4. Run `npm start` to start interacting with Miku
