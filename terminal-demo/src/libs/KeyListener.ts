import { terminal as term } from "terminal-kit";

interface KeyListenerProps {
  startKey: string,
  endKey: string,
  onStart: () => void
  onEnd: () => void
}

export default class KeyListener {
  private started: boolean = false;

  constructor(props: KeyListenerProps) {
    term.grabInput( { mouse: 'button' });
    term.on('key', (name: string) => {
      if (name == props.startKey && !this.started) {
        this.started = true;
        props.onStart();
      }
      
      if (name == props.endKey && this.started) {
        this.started = false;
        props.onEnd();
      }

      if (name === 'CTRL_C') {
    	  term.grabInput(false) ;
        setTimeout( function() { process.exit() } , 100 ) ;
      }
    });
  }
}