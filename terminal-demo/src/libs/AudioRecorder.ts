// @ts-ignore
import _AudioRecorder from 'node-audiorecorder';
import fs from 'fs';
import path from 'path';

const TMP_DIRECTORY = 'temp'

export default class AudioRecorder {
  public isRecording: boolean = false;
  private recordingStream: fs.WriteStream | undefined;
  private recordingFilename: string = '';
  // @ts-ignore
  private recorder: _AudioRecorder;
  
  constructor() {
    if (!fs.existsSync(TMP_DIRECTORY)) {
      fs.mkdirSync(TMP_DIRECTORY);
    }

    this.recorder = new _AudioRecorder({
      program: 'sox',
      silence: 0
    });
  }

  start() {
    this.recordingFilename = path.join(
      TMP_DIRECTORY,
      Math.random()
        .toString(36)
        .replace(/[^0-9a-zA-Z]+/g, '')
        .concat('.wav')
    );
    this.recordingStream = fs.createWriteStream(this.recordingFilename, { encoding: 'binary' });
    this.recorder.start().stream().pipe(this.recordingStream);
    this.isRecording = true;
  }

  end(): string {
    this.isRecording = false;
    const _recordingFilename = this.recordingFilename;
    this.recorder.stop();
    process.stdin.resume();
    return _recordingFilename;
  }
}