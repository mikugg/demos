import * as MikuCore from '@mikugg/core';
import * as MikuExtensions from '@mikugg/extensions';
import fs from 'fs';
require('dotenv').config();
var player = require('play-sound')({})
import { Terminal, terminal as term } from "terminal-kit";
import KeyListener from './libs/KeyListener';
import AudioRecorder from './libs/AudioRecorder';

class SpinnerManager {
  private spinner: any;

  public async getSpinner(): Promise<Terminal.AnimatedText> {
    if (!this.spinner) {
      this.spinner = await term.spinner();
    }

    return this.spinner;
  }

  public async newSpinner() {
    return this.spinner = await term.spinner();
  }
}

const spinnerManager = new SpinnerManager();


// SETUP CHAT
class DialogLogger extends MikuCore.OutputListeners.OutputListener<MikuCore.OutputListeners.DialogOutputEnvironment> {
  public async handleOutput(output: MikuCore.OutputListeners.DialogOutputEnvironment): Promise<void> {
    const spinner2 = (await spinnerManager.getSpinner());
    await spinner2.animate(0);
    term.backDelete();
    term.green("\nMiku: ").gray(output.text).eraseLineAfter();
    term.magenta("\nYou: ");
  }
}

const chatGPTPromptCompleter = new MikuExtensions.ChatPromptCompleters.ChatGPTPromptCompleter({
  openAIKey: process.env.OPENAI_API_KEY || '',
  basePrompt: fs.readFileSync(__dirname + '/assets/prompt.txt', 'utf8'),
  language: 'en',
  subjects: ['Fuutarou'],
  botSubject: 'Miku',
});

const whisperCommandGenerator = new MikuExtensions.CommandGenerators.WhisperGradioCommandGenerator({
  apiEndpoint: process.env.WHISPER_AUDIO_API || '',
  apiKey: process.env.WHISPER_AUDIO_API_KEY || '',
});

whisperCommandGenerator.subscribe(async (command: MikuCore.Commands.Command) => {
  (await spinnerManager.getSpinner()).animate(0);
  term.backDelete();
  term.white(command.input.text);
  (await spinnerManager.newSpinner());
});


const miku = new MikuCore.ChatBot({
  promptCompleter: chatGPTPromptCompleter,
  commandGenerators: [
    whisperCommandGenerator,
  ],
  outputListeners: {
    dialogOutputListeners: [
      new DialogLogger(),
      new MikuExtensions.OutputListeners.ElevenLabsOutputListener({
        apiKey: process.env.ELEVEN_KEY || '',
        voiceId: process.env.ELEVEN_VOICE_KEY || '',
        onAudioFileSynthesized(base64: string) {
          fs.writeFileSync('temp/output.mp3', base64, {encoding: 'base64'});
          player.play('temp/output.mp3', () => {});
        }
      })
    ]
  }
});


// TERMINAL CHAT INTERACTOR
(async function() {
  term("Start chatting with ").green("Miku\n");

  await term.drawImage(__dirname + "/assets/image.png", {
    shrink: { width: 30, height: 30 },
  });
  term.blue('Press "e" to start recording, end with "w"...\n');

  const audioRecorder = new AudioRecorder();
  term.magenta("\nYou: ");

  new KeyListener({
    startKey: "e",
    endKey: "w",
    onStart: () => {
      audioRecorder.start();
    },
    onEnd: async () => {
      const filename = audioRecorder.end();
      spinnerManager.newSpinner();
      whisperCommandGenerator.emit({
        base64Input: 'data:audio/mp3;base64,' +fs.readFileSync(filename, {encoding: 'base64'}),
        commandType: MikuCore.Commands.CommandType.DIALOG
      });
    },
  });
})();