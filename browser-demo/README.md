## Miku demo for browser


#### installation
```
yarn install
```

#### environment
Create a `.env` file in the root of the project

```
VITE_OPENAI_API_KEY=openai-key
VITE_ELEVEN_KEY=your-key
VITE_ELEVEN_VOICE_KEY=your-voice-id
```

#### run
```
yarn dev
```
