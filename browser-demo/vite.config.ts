import { fileURLToPath, URL } from 'url'
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
import { NodeModulesPolyfillPlugin } from '@esbuild-plugins/node-modules-polyfill'
import plainText from 'vite-plugin-plain-text';


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), plainText([/\/LICENSE$/, '**/*.text', /\.glsl$/]),],
  define: {
    'setImmediate': 'setTimeout.bind(null)',
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      util: 'rollup-plugin-node-polyfills/polyfills/util'
    }
  },
  server: {
    proxy: {
      "/elevenlabs": {
        target: "https://api.elevenlabs.io",
        changeOrigin: true,
        secure: false,
        rewrite: (path) => path.replace(/^\/elevenlabs/, ""),
      },
    },
  },
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
            NodeGlobalsPolyfillPlugin({
                process: true,
                buffer: true
            }),
            NodeModulesPolyfillPlugin()
      ]
    }
  }
});
