import { useEffect, useState } from "react";
import "./aside.css";
import mikugg from "../../libs/mikugg";

export const Aside = () => {
  const [history, setHistory] = useState<string>(mikugg.getCurrentPrompt());

  useEffect(() => {
    mikugg.subscribeDialog(() => {
      console.log('22', mikugg.getCurrentPrompt());
      setHistory(mikugg.getCurrentPrompt());
    })
  }, [])

  return (
    <div className="w-1/4 h-full bg-sky-500 text-white border-l-[1px] border-sky-600">
      {/* BUTTONS */}
      <div className="flex justify-center items-center gap-12 w-full h-1/6">
        <button className="font-bold rounded-full border border-solid pl-6 pr-6 pt-2 pb-2 hover:bg-sky-700 hover:border-none">HISTORY</button>
        <button className="font-bold rounded-full border border-solid pl-6 pr-6 pt-2 pb-2 hover:bg-sky-700 hover:border-none">LOGS</button>
      </div>
      {/* HISTORY AND LOGS */}
      <div className="scrollbar flex justify-center w-full h-5/6 p-3 overflow-auto">
        <textarea value={history} className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" disabled ></textarea>
      </div>
    </div>
  );
};
