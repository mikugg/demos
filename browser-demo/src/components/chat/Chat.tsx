import { useState } from "react";
import Paper from "../../assets/paper.svg";
import { DropDown } from "../dropdown/Dropdown";
import mikugg from '../../libs/mikugg';

export const Chat = () => {
  const [value, setValue] = useState('');

  const sendPrompt = (event: React.FormEvent): void => {
    event.preventDefault();
    mikugg.sendPrompt(value);
    setValue('');
  };

  return (
    <div className="flex items-center justify-center w-full h-1/4 gap-5">
      <DropDown />
      <form
        onSubmit={sendPrompt}
        className="flex items-center justify-center relative shadow-[0_0_10px_rgba(0,0,0,0.10)] w-3/4 h-1/4 shadow-sm placeholder:italic placeholder:text-slate-400 block bg-white border border-slate-300 rounded-md shadow-sm focus-within:outline-none focus-within:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm">
        <input
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="bg-white w-full h-full ml-4 focus:outline-none"
          type="text"
          name="search"
          placeholder="Start chatting..."
        />
        <label className="w-1/8 h-1/2 mr-4 text-blue-700">
          <img className="w-full h-full cursor-pointer fill-red-900" src={Paper} />
          <input type="submit" className="hidden" />
        </label>
      </form>
    </div>
  );
};
