import "./conversation.css";
import Miku from "../../assets/miku.png";
import { useEffect, useState } from "react";
import mikugg from "../../libs/mikugg";

export const Conversation = () => {
  const [message, setMessage] = useState<string>();

  useEffect(() => {
    console.log('subscribe dialog 3');
    mikugg.subscribeDialog((output) => {
      console.log('subscribe dialog 2');
      setMessage(output.text);
    })
  }, [])

  useEffect(() => {
    mikugg.subscribeAudio((base64: ArrayBuffer) => {
      function _arrayBufferToBase64( buffer: ArrayBuffer ) {
        var binary = '';
        var bytes = new Uint8Array( buffer );
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return window.btoa( binary );
      }
      const snd = new Audio("data:audio/mp3;base64," + _arrayBufferToBase64(base64));
      snd.play();
    })
  }, [])

  return (
    // MAIN CONTAINER
    <div className="flex flex-col w-full h-3/4">
      {/* MAIN IMAGE */}
      <div className="flex justify-center items-center w-full h-3/4">
        <img className="h-[60vh]" src={Miku} alt="logo" />
      </div>
      {/* CONVERSATION CONTAINER */}
      <div className="flex justify-center items-center w-full h-1/4">
        {/* CONVERSATION */}
        <div className="scrollbar w-11/12 h-full p-3 block bg-white border border-slate-300 rounded-md overflow-auto">
          {message}
        </div>
      </div>
    </div>
  );
};
