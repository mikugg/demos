import { Conversation } from "./components/conversation/Conversation";
import { Chat } from "./components/chat/Chat";
import { Aside } from "./components/aside/Aside";

export const App = () => {
  return (
    <div className="flex w-full h-screen">
      <div className="w-3/4 h-full">
        <Conversation />
        <Chat />
      </div>
      <Aside />
    </div>
  );
};

export default App;
