import * as MikuCore from '@mikugg/core';
import * as MikuExtensions from '@mikugg/extensions';
import { plainText as basePrompt } from '../assets/promptBase.text';
import { plainText as starterPrompt } from '../assets/promptStarter.text';

interface MikuGGConfig {
  openaiKey: string
  elevenlabsKey: string
  elevenlabsVoiceID: string
}

class MikuGG {
  private config: MikuGGConfig;
  private textWriter: MikuCore.CommandGenerators.TextCommandGenerator;
  private dialogLogger: MikuCore.OutputListeners.SimpleListener<MikuCore.OutputListeners.DialogOutputEnvironment>;
  private audioLogger: MikuExtensions.OutputListeners.ElevenLabsOutputListener;
  private chatGPT: MikuExtensions.ChatPromptCompleters.ChatGPTPromptCompleter;

  constructor(config: MikuGGConfig) {
    this.config = config;
    this.textWriter = new MikuCore.CommandGenerators.TextCommandGenerator();
    this.dialogLogger = new MikuCore.OutputListeners.SimpleListener<MikuCore.OutputListeners.DialogOutputEnvironment>();
    this.chatGPT = new MikuExtensions.ChatPromptCompleters.ChatGPTPromptCompleter({
      openAIKey: this.config.openaiKey,
      basePrompt: basePrompt,
      startPrompt: starterPrompt,
      language: 'en',
      subjects: ['Fuutarou'],
      botSubject: 'Miku',
    });

    this.audioLogger = new MikuExtensions.OutputListeners.ElevenLabsOutputListener({
      apiUrl: '/elevenlabs/v1/text-to-speech',
      apiKey: this.config.elevenlabsKey || '',
      voiceId: this.config.elevenlabsVoiceID || '',
    });

    new MikuCore.ChatBot({
      promptCompleter: this.chatGPT,
      commandGenerators: [this.textWriter],
      outputListeners: {
        dialogOutputListeners: [
          this.dialogLogger,
          this.audioLogger
        ]
      }
    });
  }

  subscribeDialog(cb: (output: MikuCore.OutputListeners.DialogOutputEnvironment) => void) {
    this.dialogLogger.subscribe(cb);
  }
  
  subscribeAudio(cb: (buffer: ArrayBuffer) => void) {
    this.audioLogger.subscribe(cb);
  }

  sendPrompt(text: string): Promise<void> {
    return this.textWriter.emit({
      type: MikuCore.Commands.CommandType.DIALOG,
      input: {
        text,
        subject: 'Fuutarou'
      }
    });
  }

  getCurrentPrompt() {
    return this.chatGPT.getCurrentPrompt();
  }
}

export default new MikuGG({
  openaiKey: import.meta.env.VITE_OPENAI_API_KEY || '',
  elevenlabsKey: import.meta.env.VITE_ELEVEN_KEY || '',
  elevenlabsVoiceID: import.meta.env.VITE_ELEVEN_VOICE_KEY || '',
});